# Marvel Characters
An application with basically two pages, the first one displaying a list of Marvel characters, the second page displaying the selected character details, such as series, the possibility to edit the character info and save it locally.
The idea of this project is for you understand a little better my of programming skills and style.

## Getting Started

**Requirements** <br>
You need Node and Git

> git clone https://gitlab.com/limaneto/marvels-challenge.git<br>
> cd marvels-challenge<br>

As the Marvel characters providers I used the [Marvel API](https://developer.marvel.com/docs)<br>
To connect to the Marvel API you need to create a new file called `config.js` inside `src/utils/` based on the `config.example.js` file that is also inside `src/utils/` folder.<br>
It will ask for a private key and a public key.<br>
You can generate both at the [Marvel Website](https://www.marvel.com/signin?referer=https%3A%2F%2Fdeveloper.marvel.com%2Faccount).<br>
If you don't feel like it, you can use mine:<br>
> privateKey: 843975b8707b3d81a3266c1b4d17228cfeb056c2<br>
> publicKey: ed9fc0b8f2563ea3ce286718e4bd4de2<br>

> npm install<br>
> npm start


  if everything goes well the application is going to open by itself at [http://localhost:3000/characters](http://localhost:3000/characters)
  

## Available Scripts

  

In the project directory, you can run:
### `npm start`
Runs the app in the development mode.
Open [http://localhost:3000/characters](http://localhost:3000/characters) to view it in the browser.
