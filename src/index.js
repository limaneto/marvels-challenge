import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import {
  Route,
  Switch,
  Redirect,
  BrowserRouter as Router,
} from 'react-router-dom';
import {
  Characters,
  CharacterDetails,
} from './screens';
import { Header } from './components';
import reducers from './state/reducers';
import './assets/fonts/Sen/Sen-Regular.ttf'
import './assets/fonts/Sen/Sen-Bold.ttf'
import './reset.css';
import './index.css';
import * as serviceWorker from './serviceWorker';

const store = createStore(reducers, applyMiddleware(thunk));


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
        <Redirect exact from="/" to="characters" />
        <Header />
        <Route>
        </Route>
        <Switch>
          <Route path={'/characters/:id'}>
            <CharacterDetails />
          </Route>
          <Route path={'/characters'}>
            <Characters />
          </Route>
        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
