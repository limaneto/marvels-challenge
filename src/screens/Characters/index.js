import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { CharacterCard, Banner, BaseButton, Loading } from '../../components';
import { searchCharacterAction, fetchCharactersAction, nextPageAction } from '../../state/actions';
import { colors } from '../../utils/constants';
import useDebounce from '../../utils/useDebounce';

const ListContainer = styled.div`
  display: grid;
  grid-gap: 25px;
  grid-template-columns: repeat(auto-fill, minmax(200px, auto));
  padding: 20px;
`;

const Input = styled.input`
  margin-left: 20px;
  border-top: none;
  border-right: none;
  border-left: none;
  border-bottom-color: ${colors.red};
`;

const SearchStatus = styled.p`
  display: inline-block;
  position: absolute;
  right: 20px;
  font-size: 1.2em;
}
  
  @media screen and (max-width: 420px) {
    font-size: 1em;
  }
`;

const Characters = ({ search = '', charactersStore, searchCharacter, fetchCharacters, nextPage }) => {
  const { charactersList, pagination } = charactersStore;
  const [searchName, setSearchName] = useState(search);
  const [fetching, setFetching] = useState(false);
  const [searching, setSearching] = useState(false);
  const [fetchingNext, setFetchingNext] = useState(false);
  const debouncedSearchName = useDebounce(searchName, 500);

  useEffect(() => {
    if (charactersList.length) return;
    (async () => {
      try {
        setFetching(true);
        await fetchCharacters();
      } catch (e) {
        console.log(e);
      } finally {
        setFetching(false);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      setSearching(true);
      await searchCharacter(debouncedSearchName);
      setSearching(false);
    })();
  }, [debouncedSearchName]);

  const getNextPage = async () => {
    try {
      setFetchingNext(true);
      await nextPage();
    } catch (e) {
      console.log(e);
    } finally {
      setFetchingNext(false);
    }
  };

  return (
    <div>
      <Banner />
      <Input type="text" placeholder='Find character' value={searchName} onChange={(e) => setSearchName(e.target.value)}/>
      {
        searching ? <SearchStatus>Searching...</SearchStatus> : <SearchStatus>{`${pagination.total} result${pagination.total === 1 ? '' : 's'}`}</SearchStatus>
      }
      {
        fetching && <Loading />
      }
      <ListContainer>
        {
          charactersList.map(item => (
            <CharacterCard key={item.id} character={item} />
          ))
        }
      </ListContainer>
      {
        pagination.total > charactersList.length &&
        <BaseButton onClick={getNextPage}>{fetchingNext ? 'Loading More ...' : 'Next'}</BaseButton>
      }
    </div>
  )
};

Characters.propTypes = {
  charactersStore: PropTypes.object.isRequired,
  searchCharacter: PropTypes.func,
  fetchCharacters: PropTypes.func,
  nextPage: PropTypes.func,
};

Characters.defaultProps = {
  searchCharacter: () => {},
  fetchCharacters: () => {},
  nextPage: () => {},
};

const mapStateToProps = (state) => {
  return {
    search: state.search,
    charactersStore: state.charactersStore,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    searchCharacter: (search) => dispatch(searchCharacterAction(search)),
    fetchCharacters: page => dispatch(fetchCharactersAction(page)),
    nextPage: () => dispatch(nextPageAction()),
  }
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Characters);
