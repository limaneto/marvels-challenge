import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Banner, SerieCard, BaseButton, Loading } from '../../components';
import { AvatarWrapper, Container, ListContainer, Title, Image } from './style';
import Form from './Form';
import { editCharacterAction } from '../../state/actions';
import axios from "../../utils/axios";

const Avatar = (props) => {
  const { character } = props;
  const { thumbnail } = character;
  return (
    <AvatarWrapper>
      <Image src={`${thumbnail.path}.${thumbnail.extension}`} alt="character"/>
    </AvatarWrapper>
  )
};

const CharacterDetails = ({ characters, editCharacter }) => {
  const { id } = useParams();
  const selectedCharacter = characters.find(item => item.id === parseInt(id));
  const [series, setSeries] = useState([]);
  const [fetching, setFetching] = useState(false);
  const [showForm, setShowForm] = useState(false);


  const toggleForm = () => {
    setShowForm(!showForm);
  };

  const submitForm = (values) => {
    editCharacter({ ...selectedCharacter, ...values });
    toggleForm();
  };

  const fetchSeries = async (series) => {
    const seriesId = series.map(serie => {
      const paths = serie.resourceURI.split('/');
      return paths[paths.length - 1];
    });
    const requests = seriesId.map(id => axios.get(`series/${id}`));
    return await axios.all(requests);
  };

  useEffect(() => {
    (async () => {
      try {
        setFetching(true);
        const response = await fetchSeries(selectedCharacter.series.items || []);
        const fetchedSeries = response.map(item => item.data.data.results[0]);
        setSeries(fetchedSeries)
      } catch (e) {
        console.log(e);
      } finally {
        setFetching(false);
      }
    })()
  }, []);

  const values = { name: selectedCharacter.name, description: selectedCharacter.description };
  return (
    <Container>
      <Banner title={selectedCharacter.name} subTitle={selectedCharacter.description} />
      <Avatar character={selectedCharacter} />
      <div>
        <BaseButton onClick={toggleForm}>Edit Character</BaseButton>
        {
          showForm &&
           <Form values={values} onCancel={toggleForm} handleSubmit={submitForm} />
        }
      </div>
      <Title>Series</Title>
      {
        fetching &&
        <Loading />
      }
      <ListContainer>
        {
          series.length === 0 && !fetching && <p>No series found</p>
        }
        {
          series.map(serie => (<SerieCard key={serie.id} serie={serie} />))
        }
      </ListContainer>
    </Container>
  )
};

CharacterDetails.propTypes = {
  characters: PropTypes.array.isRequired,
  editCharacter: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    characters: state.charactersStore.charactersList,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    editCharacter: character => dispatch(editCharacterAction(character))
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CharacterDetails);
