import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { BaseButton } from '../../../components';
import { colors } from '../../../utils/constants';

const Background = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background-color: rgb(41, 40, 38, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
`;


const FormWrapper = styled.form`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  min-width: 500px;
  background-color: ${colors.white};
  border-radius: 5px;
  
  @media screen and (max-width: 700px) {
    min-width: 300px;
  }
`;

const InputWrapper = styled.div`
  align-items: center;
  display: flex;
  width: 80%;
  height: 100px;
  flex-direction: column;
  justify-content: space-evenly;
`;

const Input = (props) => {
  return (
    <InputWrapper>
      <label htmlFor="name">Name</label>
      <input
        type='text'
        name='name'
        id='name'
        {...props}
        style={{ width: '100%', textAlign: 'center' }}
      />
    </InputWrapper>
  );
};

const TextWrapper = styled(InputWrapper)`
  margin-bottom: 80px;
  width: 80%;
  min-height: 200px;
`;

const TextArea = styled.textarea`
  height: 100%;
  width: 100%;
  margin-top: 20px;
`;

const Text = (props) => {
  const { value, onChange } = props;
  return (
    <TextWrapper>
      <label htmlFor="description">Description</label>
      <TextArea
        name='description'
        id='description'
        onChange={onChange}
        value={value}
      />
    </TextWrapper>
  );
};

const Bottom = styled.div`
  display: flex;
  justify-content: space-evenly;
  position: absolute;
  bottom: 0;
  width: 100%;
`;

const Form = (props) => {
  let {
    handleSubmit,
    onCancel,
    values,
  } = props;

  const [name, setName] = useState(values.name);
  const [description, setDescription] = useState(values.description);
  const [submitting, setSubmitting] = useState(false);

  const submit = (e) => {
    e.preventDefault();
    setSubmitting(!submitting);
    handleSubmit({ name, description });
    setSubmitting(!submitting);
  };

  return (
    <Background>
      <FormWrapper onSubmit={submit}>
        <Input
          onChange={(e) => setName(e.target.value)}
          value={name}
        />
        <Text
          onChange={(e) => setDescription(e.target.value)}
          value={description}
        />
        <Bottom>
          <BaseButton type="submit" disabled={submitting}>
            {
              submitting ? 'Saving...' : 'Save'
            }
          </BaseButton>
          <BaseButton onClick={onCancel} disabled={submitting}>
            Cancel
          </BaseButton>
        </Bottom>
      </FormWrapper>
    </Background>
  )
};

Form.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  values: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
  }),
};

export default Form;
