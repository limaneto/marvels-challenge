import styled from "styled-components";
import { colors } from '../../utils/constants';

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
`;

export const ListContainer = styled.div`
  display: grid;
  grid-gap: 25px;
  grid-template-columns: repeat(auto-fill, minmax(300px, auto));
  padding: 20px;
`;

export const Title = styled.h1`
  font-size: 1.7em;
  text-align: center;
  margin: 30px 0 0 0;
  font-family: Sen-Bold;
  text-transform: uppercase;
  color: ${colors.black};
  
  
  @media screen and (min-width: 620px) {
    text-align: left;
    font-size: 2.1em;
    margin: 30px 0 0 20px;
  }
`;

export const AvatarWrapper = styled.div`
  display: flex;
  justify-content: center;
` ;

export const Image = styled.img`
  width: 20vw;
  height: 20vw;
  border-radius: 20vw;
  border: 2px solid ${colors.red};
  object-fit: cover;
  
  @media screen and (min-width: 620px) {
    max-width: 230px;
    max-height: 230px;
  }
`;
