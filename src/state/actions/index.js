import axios from '../../utils/axios';
import { LIMIT } from '../../utils/constants';

export const nextPageAction = () => {
  return async (dispatch, getState) => {
    const { search, charactersStore: { pagination } } = getState();
    dispatch({ type: 'CHANGE_PAGINATION', page: pagination.page + 1 });
    if (search) {
      return dispatch(searchCharacters(pagination.page + 1));
    } else {
      return dispatch(fetchCharactersAction(pagination.page + 1));
    }
  }
};

export const searchCharacterAction = (search) => {
  return dispatch => {
    dispatch({ type: 'SEARCH', search });
    if (search) {
      return dispatch(searchCharacters(1, search));
    } else {
      return dispatch(fetchCharactersAction(1));
    }
  }
};

const searchCharacters = (page, newSearch) => {
  return (dispatch, getState) => {
    const { search, charactersStore: { pagination, charactersList } } = getState();
    const newPage = page || pagination.page;

    const params = {
      ...axios.defaults.params,
      nameStartsWith: newSearch || search,
      offset: (newPage - 1) * LIMIT,
    };

    return axios.get('characters', { params })
      .then(({ data: { data } }) => {
        const results = data.results || [];
        const newPagination = { total: data.total, page: newPage, LIMIT };
        const { editedCharacters } = getState();
        const newCharactersList = swapEditedCharacters(editedCharacters, results);
        return dispatch({
          type: 'SET_CHARACTERS',
          pagination: newPagination,
          charactersList: newPage === 1 ? newCharactersList : [...charactersList, ...newCharactersList],
        })
      });
  }
};

export const fetchCharactersAction = (page) => {
  return async (dispatch, getState) => {
    const { editedCharacters, charactersStore: { pagination, charactersList } } = getState();
    const params = { ...axios.defaults.params };
    const newPage = page || pagination.page;
    params.offset = (newPage - 1) * LIMIT;
    try {
      const response = await axios.get('characters', { params });
      const { data } = response.data || {};
      const results = data.results || [];
      const newPagination = { total: data.total, page: newPage, LIMIT };
      const newCharactersList = swapEditedCharacters(editedCharacters, newPage === 1 ? results : [...charactersList, ...results]);
      return dispatch({
        type: 'SET_CHARACTERS',
        pagination: newPagination,
        charactersList: newCharactersList,
      })
    } catch (e) {
      console.log(e);
    }
  }
};

const swapEditedCharacters = (editedCharacters, charactersList) => {
  return charactersList.map(item => {
    const character = editedCharacters.find(editedCharacter => editedCharacter.id === item.id);
    if (character) {
      return {
        ...character,
      }
    }
    return item;
  })
};

const addCharacterToEditedList = (character, editedCharacters) => {
  let newEditedCharacters = [...editedCharacters];
  newEditedCharacters = newEditedCharacters.filter(item => item.id !== character.id);
  newEditedCharacters.push(character);
  return newEditedCharacters;
};

export const editCharacterAction = (character) => {
  return (dispatch, getState) => {
    const { editedCharacters, charactersStore: { pagination, charactersList } } = getState();
    const newEditedCharacters = addCharacterToEditedList(character, editedCharacters);
    const newCharactersList = swapEditedCharacters(newEditedCharacters, charactersList);
    dispatch({ type: 'SET_CHARACTERS', pagination, charactersList: newCharactersList });
    dispatch({ type: 'EDIT', newEditedCharacters });
  }
};
