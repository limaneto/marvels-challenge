const initialState = {
  editedCharacters: [],
  search: '',
  charactersStore: {
    pagination: {
      page: 1,
      limit: 20,
    },
    charactersList: [],
  },
};

const character = (state = initialState, action) => {
  switch(action.type) {
    case 'EDIT':
      return {
        ...state,
        editedCharacters: [...action.newEditedCharacters]
      };
    case 'SET_CHARACTERS':
      return {
        ...state,
        charactersStore: {
          pagination: action.pagination,
          charactersList: [...action.charactersList],
        },
      };
    case 'CHANGE_PAGINATION':
      return {
        ...state,
        charactersStore: {
          ...state.charactersStore,
          pagination: { ...state.charactersStore.pagination, page: action.page },
        },
      };
    case 'SEARCH':
      return {
        ...state,
        search: action.search,
      };
    default:
      return state;
  }
};

export default character;
