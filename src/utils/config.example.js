import md5 from 'md5';

/**
  Create a new file called config.js based on this file
  and fill the information bellow with real values
*/

export const privateKey = '';
export const publicKey = '';
export const ts = new Date().getTime();
export const hash = md5(`${ts}${privateKey}${publicKey}`);
