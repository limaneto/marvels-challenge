export const BASE_URL = 'https://gateway.marvel.com:443/v1/public';
export const LIMIT = 20;


export const colors = {
  basicBlack: 'black',
  black: '#292826',
  red: '#ED1D24',
  white: 'white',
};
