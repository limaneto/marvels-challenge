import axios from 'axios';
import {
  BASE_URL
} from './constants';
import {
  publicKey,
  ts,
  hash,
} from './config';


axios.defaults.baseURL = BASE_URL;

axios.interceptors.request.use((config) => {
  config.params = config.params || {};
  config.params.ts = ts;
  config.params.apikey = publicKey;
  config.params.hash = hash;

  return config;
});


export default axios;
