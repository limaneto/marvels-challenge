import styled from 'styled-components';
import { colors } from '../../utils/constants';

const BaseButton = styled.button`
  margin: 20px;
  background-color: ${colors.red};
  min-width: 100px;
  height: 40px;
  color: ${colors.white};
  border-radius: 5px;
  border: none;
  
  &:hover {
   opacity: 0.8;
  }
`;

export default BaseButton;
