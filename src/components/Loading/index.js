import styled from 'styled-components';
import React from 'react';

const LoadingComponent = styled.p`
  font-size: 2em;
  font-size: Sen-Bold;
  margin: 20px;
`;

const Loading = () => {
  return (
    <LoadingComponent>Loading...</LoadingComponent>
  )
};

export default Loading;
