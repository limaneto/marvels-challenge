import React from 'react';
import styled from 'styled-components';
import { colors } from '../../utils/constants';
import logo from '../../assets/images/marvel-logo.png';

const Container = styled.header`
  width: 100%;
  background-color: ${colors.black};
`;

const LogoContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 10vh;
  padding: 5px 0;
  
  @media screen and (max-width: 620px) {
    height: 8vh;
  }
`;

const Logo = styled.img`
  width: 100px;
  height: 60px;
  
  @media screen and (max-width: 620px) {
    height: 50px;
  }
`;

const Header = () => {
  return (
    <Container>
      <LogoContainer>
        <Logo src={logo} alt='character'/>
      </LogoContainer>
    </Container>
  )
};

export default Header;
