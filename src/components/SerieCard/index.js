import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { colors } from '../../utils/constants';


const Wrapper = styled.div`
  display: flex;
  align-items: center;
  min-width: 150px;
  height: 150px;
  background-color: #292826;
  
  & > div {
    width: 100%;
    height: 100%;
  }
  
  &:hover > div {
    background-color: ${colors.red};
  }
  
  & p {
    margin: 10px;
    color: ${colors.white}; 
  }
  
  & > img {
    object-fit: cover;
    width: 40%;
    height: 100%;
    border-right: 2px solid red;
  }  
`;



const SerieCard = (props) => {
  const { serie: { title, thumbnail } } = props;
  return (
    <Wrapper>
      <img src={`${thumbnail.path}.${thumbnail.extension}`} alt='Serie' />
      <div>
        <p>{title}</p>
      </div>
    </Wrapper>
  )
};

SerieCard.propTypes = {
  serie: PropTypes.shape({
    title: PropTypes.string.isRequired,
    thumbnail: PropTypes.object,
  }),
};

export default SerieCard;
