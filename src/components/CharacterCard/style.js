import styled from 'styled-components';
import { colors } from '../../utils/constants';
import { Link as BaseLink } from "react-router-dom";

export const Container = styled.div`
  position: relative;
  background-color: ${colors.black};
  width: 100%;
  height: 100%;
  max-height: 350px;
  display: inline-block;
  border: 1px solid ${colors.black};
  border-bottom: 0;
  
  &:hover {
    background-color: ${colors.red};
  }
`;

export const CharacterName = styled.p`
  position: absolute;
  left: 10px;
  bottom: 10px;
  color: ${colors.white};
  font-size: 0.8em;
  font-family: Sen-Bold;
  text-transform: uppercase;
`;

export const Cut = styled.div`
  position: absolute;
  right: -11px;
  bottom: -10px;
  width: 0;
  height: 0;
  border-left: 15px solid transparent;
  border-right: 15px solid transparent;
  border-bottom: 15px solid ${colors.white};
  -webkit-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  transform: rotate(145deg);
`;

export const Image = styled.img`
  width: 100%;
  height: 80%;
  object-fit: cover;
  border-bottom: 3px solid ${colors.red};
`;

export const Link = styled(BaseLink)`
  height: 250px;
  
  @media screen and (max-width: 900px) {
    height: 180px;
  }
`;
