import React from 'react';
import PropTypes from 'prop-types';
import { Container, CharacterName, Cut, Image, Link } from './style';

const CharacterCard = (props) => {
  const { character: { name, thumbnail, id } } = props;
  return (
    <Link to={`/characters/${id}`}>
      <Container>
        <Image src={`${thumbnail.path}.${thumbnail.extension}`} alt='Character'/>
        <CharacterName>{name}</CharacterName>
        <Cut />
      </Container>
    </Link>
  )
};

CharacterCard.propTypes = {
  character: PropTypes.shape({
    name: PropTypes.string.isRequired,
    thumbnail: PropTypes.shape({
      path: PropTypes.string.isRequired,
    })
  })
};



export default CharacterCard;
