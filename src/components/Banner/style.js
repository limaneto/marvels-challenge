import styled from 'styled-components';
import { colors } from '../../utils/constants';

export const Wrapper = styled.section`
  width: 100%;
  height: 45vh;
  background-color: ${colors.black};
  margin-bottom: 20px;
  
  @media screen and (max-width: 420px) {
    height: 30vh;
  }
`;

export const Image = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  padding: 30px;
  height: 90%;
  text-align: center;
  background-image: url(${props => props.src ? props.src : 'none'}); 
  box-shadow: 0 0 100px 30px ${colors.basicBlack} inset;
`;

export const Title = styled.h1`
  font-family: Sen-Bold;
  font-size: 2em;
  color: ${colors.white};
  margin: 10px 0;
  
  @media screen and (max-width: 420px) {
    font-size: 1.4em;
  }
`;

export const Subtitle = styled.h3`
  font-size: 1em;
  color: ${colors.white};
  overflow: scroll;
  
  @media screen and (max-width: 420px) {
    font-size: 0.8em;
  }
`;

