import React from 'react';
import PropTypes from 'prop-types';
import { Wrapper, Image, Title, Subtitle } from './style';
import land from '../../assets/images/land.jpg';

const Banner = (props) => {
  const { title, subTitle, image } = props;
  return (
    <Wrapper>
      <Image src={image}>
        <Title>{title}</Title>
        <Subtitle>{subTitle}</Subtitle>
      </Image>
    </Wrapper>
  )
};

Banner.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  image: PropTypes.node,
};

Banner.defaultProps = {
  title: 'Marvel Characters',
  subTitle: 'Get hooked on a hearty helping of heroes and villains from the humble House of Ideas!',
  image: land,
};

export default Banner;
